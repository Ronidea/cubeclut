#!./cubenv/bin/python3
import argparse, time
from tqdm import tqdm
from concurrent import futures
from concurrent.futures import ProcessPoolExecutor, as_completed
from pathlib import Path
from PIL import Image
from pillow_lut import load_cube_file

def parse_arguments():    
    parser = argparse.ArgumentParser(prog="cubetoclut", description="Convert 3d-LUT .cube files to haldCLUT format, used by RawTherapees film simulation tool.", epilog="If you have money to spare, donate to Free/OpenSource projects.")
    parser.add_argument("cube", type=Path, help="A .cube file or directory containing cube files that should be converted to haldCLUT format used by RawTherapee film simulation.")
    parser.add_argument("--out", type=Path, default=None,
                        help="Target file/dir for haldCLUT(s).\nIf no out argument is specified, the program will save the haldCLUT tif image under the same name and directory as the corresponding .cube file (with different file extention of course).\nIf the out argument is given as a filename, it will be used to save the haldCLUT tif or png image (depending on the filename).\nIf the out argument is given as a directory name, the program will save the haldCLUT tif file inside that directory under the same name as the corresponding .cube file.")
    parser.add_argument("--identity", type=Path, default="./Identity_12.tif",
                        help="Custom haldCLUT identity file (if neccessary for whatever reason).")
    return parser.parse_args()

def convert_and_save(fp_cube, identity_clut, out):
    if out == None:
        # no out specified, save haldCLUT under same name as 3dlut file inside same dir
        out = fp_cube.with_suffix('.tif')
    elif out.is_dir():
        # out is a dir, save haldCLUT under same name as 3dlut file inside out dir
        out = out.joinpath(fp_cube.name).with_suffix('.tif')
    elif out.exists():
        # out is a file and already exists, save haldCLUT under this name, but add number
        number = 1
        # increase 'number' as long as 'out-[number]' already exists
        while out.with_name(f"{out.stem}-{number}{out.suffix}").exists():
            number += 1

        out = out.with_name(f"{out.stem}-{number}{out.suffix}")

    lut3d = load_cube_file(fp_cube.as_posix())
    identity_clut.filter(lut3d).save(out)

    return out

def main(args):
    identity_clut = Image.open(args.identity)
    cubes = list(args.cube.glob('*.cube'))

    if args.cube.is_dir():
        print(f"Processing {len(cubes)} *.cube files:")
        # tqdm progress bar *yay*
        with tqdm(total=len(cubes)) as pbar:
            with ProcessPoolExecutor(2) as executor:
                jobs = {executor.submit(
                    convert_and_save, cube, identity_clut, args.out): cube for cube in cubes}

                res = []
                for future in as_completed(jobs):
                    pbar.update(1)
                    res.append(future.result())    
    else:
        res = [convert_and_save(args.cube, identity_clut, args.out)]

    print(f"Converted {len(res)} cube files to haldCLUT format:")
    print("\n".join(f"{i}) {e.resolve()}" for i,e in enumerate(res, start=1)))

if __name__ == "__main__":
    args = parse_arguments()
    main(args)
