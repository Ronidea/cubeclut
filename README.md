# Convert .cube Files to haldCLUT.tif (or png) for Usage inside RawTherapee

This is a very basic script that applies the given 3D (cube) LookUpTable (LUT) to the haldCLUT identity file provided by RawTherapee for their film simulation tool.

## How to install

1. Install Python3 (their are lots of guides online, if needed),
2. Download the ZIP of this project and unzip it or clone it using git,
3. Open your favorite Terminal emulator and navigate in the directory of this file,0
4. Run following command, to create a Python environment: `python3 -m venv cubenv/`,
5. Run following command, to install required packages: `cubenv/bin/pip3 install pillow==9.5.0 tqdm
6. Optional: Add execute permissions (`chmod +x cubetoclut.py`) if you want a shorter command



## How to use
Note: By default the output file be saved next to the input file under the same name (but .tif instead if .cube of course)


#### Example commands
* Convert one file: `cubenv/bin/python3 cubetoclut /path/to/LUT_example.cube`
* Convert all files in a directory: `cubenv/bin/python3 cubetoclut /path/to/directory/`
* Convert and save elsewhere: `cubenv/bin/python3 cubetoclut /path/to/LUT_example.cube /another/path/LUT_example.tif`
* Convert and save as png: `cube/bin/python3 cubetoclut /path/to/LUT_example.cube /another/path/LUT_example.png`

You can also just run ./cubetoclut, if you apply execute permission. But depending on your platform this might not work straight out of the box (due to the shebang or whatever) so hence this guide uses the verbose command.

For a detailled description, run: `cubenv/bin/python3 cubetoclut --help`